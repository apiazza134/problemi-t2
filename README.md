![build status](https://gitlab.com/apiazza134/problemi-t2/badges/master/pipeline.svg)

# Problemi di Teorica 2
Scansioni delle soluzioni dei problemi 1 e 7 per l'esame di Fisica Teorica 2 (anno 2020/2021).

Il PDF si può scaricare [qui](https://gitlab.com/apiazza134/problemi-t2/-/jobs/artifacts/master/raw/problemi-t2.pdf?job=compile_pdf).
